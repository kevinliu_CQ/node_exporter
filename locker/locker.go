package locker

import "sync"

type Locker uint

var lockerInstance *Locker //0 unlocked,1 locked
var once sync.Once

func Lock() bool {
	if *lockerInstance != 0 {
		return false
	}
	*lockerInstance = 1
	return true
}
func Unlock() {
	*lockerInstance = 0

}
func init() {
	once.Do(func() {
		lockerInstance = new(Locker)
	})

}
